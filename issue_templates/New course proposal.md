Use this template to propose a new course.
# Proposed Course Title
insert a title
# Target audience
Be as specific as possible
# Audience size
Estimate the number of users who would purchase this course
# Course Outcomes
List what  a user who completes the course will be able to do. 
- What new knowledge will they possess? 
- What specific skills will they gain?
- What specific tasks will GitLab be able to claim the user can perform after completing the course?
# Duration
Estimate the number of training hours or days required to teach this course including in-class demos and lab activities
# Level
What skill level is this course (basic, intermediate, or advanced)?
# Prerequisites
What courses or other knowledge or skills should a user already possess to successfully complete this course?
# Prioritization
Please share your insights on the priority of creating and releasing this course compared to other courses.
