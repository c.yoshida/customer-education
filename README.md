## About
This project is used by GitLab team members working on Customer Enablement initiatives to track related issues.

## Project Members
Owner: @c.yoshida

## Links

[Customer Enablement Handbook](/handbook/sales/field-operations/customer-enablement/)